#include <stdio.h>

main()
{
    //Calcular a área de um triângulo
    int altura, base, area;

    printf("Qual a base do triangulo? ");
    scanf("%d", &base); //scanf exige que seja utilizado o endereço da variavel (notação &)

    printf("E a altura? ");
    scanf("%d", &altura);

    area = base*altura/2; //a = b*h/2

    printf("Area: %d", area);
}
