#include <stdio.h>

main()
{
    //Exibe as operações aritméticas com 2 numeros
    int n1 = 4, n2 = 3; //Use valores diferentes para ter novos resultados

    printf("n1 + n2 = %d", n1+n2); //%d => int | %f => float
    printf("\nn1 - n2 = %d", n1-n2);
    printf("\nn1 x n2 = %d", n1*n2);
    printf("\nn1 / n2 = %d", n1/n2); //para ter ponto flutuante, use float
    printf("\nResto da divisao: %d", n1%n2);
}
