#include <stdio.h>

main()
{
    //Este exemplo serve para ilustrar a diferença na leitura dos dados
    char c;

    printf("Insira um caractere: ");
    scanf("%c", &c);

    printf("Em char: %c", c);
    printf("\nEm int: %d", c);
}
