#include <stdio.h>
#include <string.h>

#define TAM 20

main()
{
    char nome[TAM]; //Declarado desta forma para armazenar multiplos caracteres

    printf("Qual o seu nome? ");
    scanf("%s", &nome); //%s => variavel string (sequencia de caracteres) 

    printf("Hello, %s!", nome);
}
