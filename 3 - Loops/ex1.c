#include <stdio.h>

#define TAM 10

main()
{
    int n, aux = 1;
    printf("Escolha um numero para exibir a tabuada: ");
    scanf("%d", &n);

    while(aux <= TAM)
    {
        printf("\n%d x %d = %d", n, aux, n*aux);
        aux++;
    }
}
