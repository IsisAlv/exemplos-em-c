#include <stdio.h>

#define TAM 10

main()
{
    int n, i;

    printf("Escolha um numero para exibir a tabuada: ");
    scanf("%d", &n);

    for(i = 1; i <= TAM; i++)
    {
        printf("\n%d x %d = %d", n, i, n*i);
    }
}
