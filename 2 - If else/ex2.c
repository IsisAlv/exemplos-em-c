#include <stdio.h>

main()
{
    //Compara 3 números e diz qual é o maior
    int n1, n2, n3, maior;

    printf("Insira 3 numeros para comparar\n"); // \n funciona como quebra de linha
    scanf("%d", &n1);
    scanf("%d", &n2);
    scanf("%d", &n3);

    if(n1 >= n2 && n1 >= n3) //Condição 1: se n1 é maior ou igual a n2 E n3
    {
        printf("O maior numero: %d", n1);
    }
    else if (n2 >= n1 && n2 >= n3) //Condição 2: se n2 é maior ou igual a n1 E n3
    {
        printf("O maior numero: %d", n2);
    }
    else // Se as duas condições forem falsas
    {
        printf("O maior numero: %d", n3);
    }
}
