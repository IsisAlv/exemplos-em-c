#include <stdio.h>

main()
{
    //Dizer se o número inserido é par ou ímpar
    int n1;

    printf("Insira um numero: ");
    scanf("%d", &n1);

    if(n1%2 == 0) //Condição: se o resto da divisão de n1 por 2 é igual a 0
        printf("Par!"); //Executa se a condição for verdadeira
    else
        printf("Impar!"); //Executa se a condição for falsa
}
