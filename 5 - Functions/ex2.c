#include <stdio.h>

#define SIZE 3

void set_empty(char tictactoe[SIZE][SIZE]);
void print_grid(char tictactoe[SIZE][SIZE]);
int change_player(int turn);
void show_result(int winner);
int verify_winner_game(char tictactoe[SIZE][SIZE]);
int verify_draw(char tictactoe[SIZE][SIZE]);
void play_turn(char tictactoe[SIZE][SIZE], int turn);

main()
{
    char tictactoe[SIZE][SIZE], turn='O';
    int winner = 0;

    set_empty(tictactoe);

    do
    {
        print_grid(tictactoe);
        play_turn(tictactoe, turn);
        winner = verify_winner_game(tictactoe);
        turn = change_player(turn);

        if(winner == 0)
            winner = verify_draw(tictactoe);
    }while(winner == 0);

    print_grid(tictactoe);
    show_result(winner);
}

//Torna todos os campos do tabuleiro iguais a ' ' (espaço)
void set_empty(char tictactoe[SIZE][SIZE])
{
    int l, c;
    for(l = 0; l < SIZE; l++)
    {
        for(c = 0; c < SIZE; c++)
            tictactoe[l][c] = ' ';
    }
}

//Mostra o tabuleiro na tela
void print_grid(char tictactoe[SIZE][SIZE])
{
    int l, c;
    printf("\n");
    for(l = 0; l < SIZE; l++)
    {
        if(l > 0)
            printf("\n---------\n");
        for(c = 0; c < SIZE; c++)
        {
            printf("%c", tictactoe[l][c]);
            if(c < 2)
                printf(" | ");
        }
    }
}

//Troca de quem é a vez de jogar
int change_player(int turn)
{
    if(turn == 'O')
        return 'X';
    return 'O';
}

//Mostra resultado da partida
void show_result(int winner)
{
    if(winner < 0)
        printf("\n\nDeu velha!");
    else
        printf("\n\n%c ganhou!", winner);
}

//Verifica se alguem ganhou
int verify_winner_game(char tictactoe[SIZE][SIZE])
{
    int i;
    for(i = 0; i <  SIZE; i++)
    {
        if(tictactoe[i][0] == tictactoe[i][1] && tictactoe[i][0] == tictactoe[i][2] && tictactoe[i][0] != ' ')
            return tictactoe[i][0];
        if(tictactoe[0][i] == tictactoe[1][i] && tictactoe[0][i] == tictactoe[2][i] && tictactoe[0][i] != ' ')
            return tictactoe[0][i];
    }
    if(((tictactoe[1][1] == tictactoe[0][0] && tictactoe[1][1] == tictactoe[2][2]) || (tictactoe[1][1] == tictactoe[2][0] && tictactoe[1][1] == tictactoe[0][2])) && tictactoe[1][1] != ' ')
        return tictactoe[1][1];
    return 0;
}

//Verifica se deu velha
int verify_draw(char tictactoe[SIZE][SIZE])
{
    int count = 0, l, c;
    for(l = 0; l < SIZE; l++)
    {
        for(c = 0; c < SIZE; c++)
        {
            if(tictactoe[l][c] != ' ')
                count++;
        }
    }
    if(count == SIZE*SIZE)
        return -1;
    return 0;
}

//Novo turno
void play_turn(char tictactoe[SIZE][SIZE], int turn)
{
    int validMove = 0, x, y;
    do
    {
        printf("Vez de %c: ", turn);
        scanf("%d %d", &x, &y);
        if(tictactoe[x][y] == ' ')
        {
            tictactoe[x][y] = turn;
            validMove++;
        }
        else
            printf("\nErro: Jogada invalida\n");
    }while(validMove == 0);
}
