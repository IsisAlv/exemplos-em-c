#include <stdio.h>

void mostrar_opt();
int soma(int n1, int n2);
int subtracao(int n1, int n2);
int multiplicacao(int n1, int n2);
int divisao(int n1, int n2);

int main()
{
    int opt, resultado = NULL, a, b;
    mostrar_opt();
    scanf("%d", &opt);

    printf("Insira dois numeros: ");
    scanf("%d %d", &a, &b);

    switch(opt)
    {
        case 1:
            resultado = soma(a, b);
            break;
        case 2:
            resultado = subtracao(a, b);
            break;
        case 3:
            resultado = multiplicacao(a, b);
            break;
        case 4:
            resultado = divisao(a, b);
            break;
        default:
            printf("Operacao invalida");
            break;
    }

    if(resultado)
        printf("Resultado: %d", resultado);

    return 0;
}

void mostrar_opt()
{
    printf("Qual operacao deseja realizar?\n\n");
    printf("1 - Soma\n");
    printf("2 - Subtracao\n");
    printf("3 - Multiplicacao\n");
    printf("4 - Divisao\n\n");
}

int soma(int n1, int n2)
{
    return n1+n2;
}

int subtracao(int n1, int n2)
{
    return n1-n2;
}

int multiplicacao(int n1, int n2)
{
    return n1*n2;
}

int divisao(int n1, int n2)
{
    return n1/n2;
}
