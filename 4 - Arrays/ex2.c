#include <stdio.h>
#include <string.h>

#define TAM 20

main()
{
    char nome[TAM];

    printf("Qual o seu nome? ");
    scanf("%s", &nome);

    printf("\nSeu nome contem %d letras",strlen(nome));
    printf("\nSua inicial: %c", toupper(nome[0]));
}
