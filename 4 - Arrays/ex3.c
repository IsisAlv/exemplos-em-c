#include <stdio.h>

#define SIZE 3

main()
{
    char tictactoe[SIZE][SIZE], turn='O';
    int winner = 0, l, c, x, y;

    for(l = 0; l < SIZE; l++)
    {
        for(c = 0; c < SIZE; c++)
            tictactoe[l][c] = ' ';
    }

    do
    {
        printf("\n");
        for(l = 0; l < SIZE; l++)
        {
            if(l > 0)
                printf("\n---------\n");
            for(c = 0; c < SIZE; c++)
            {
                printf("%c", tictactoe[l][c]);
                if(c < 2)
                    printf(" | ");
            }
        }

        int validMove = 0;
        do
        {
            printf("Vez de %c: ", turn);
            scanf("%d %d", &x, &y);
            if(tictactoe[x][y] == ' ')
            {
                tictactoe[x][y] = turn;
                validMove++;
            }
            else
                printf("\nErro: Jogada invalida\n");
        }while(validMove == 0);

        for(l = 0; l <  SIZE; l++)
        {
            if(tictactoe[l][0] == tictactoe[l][1] && tictactoe[l][0] == tictactoe[l][2] && tictactoe[l][0] != ' ')
            {
                winner = tictactoe[l][0];
                break;
            }
            if(tictactoe[0][l] == tictactoe[1][l] && tictactoe[0][l] == tictactoe[2][l] && tictactoe[0][l] != ' ')
            {
                winner = tictactoe[0][l];
                break;
            }
        }
        if(((tictactoe[1][1] == tictactoe[0][0] && tictactoe[1][1] == tictactoe[2][2]) || (tictactoe[1][1] == tictactoe[2][0] && tictactoe[1][1] == tictactoe[0][2])) && tictactoe[1][1] != ' ')
            winner = tictactoe[1][1];

        if(turn == 'O')
            turn = 'X';
        else
            turn = 'O';

        if(winner == 0)
        {
            int count = 0;
            for(l = 0; l < SIZE; l++)
            {
                for(c = 0; c < SIZE; c++)
                {
                    if(tictactoe[l][c] != ' ')
                        count++;
                }
            }
            if(count == SIZE*SIZE)
                winner = -1;
        }
    }while(winner == 0);

    if(winner < 0)
        printf("\n\nDeu velha!");
    else
        printf("\n\n%c ganhou!", winner);
}
